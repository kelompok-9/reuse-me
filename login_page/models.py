from django.db import models

# Create your models here.
# Data : nomor id, nama lengkap, username, email, password, alamat rumah, nama bank, rek bank

class User(models.Model):
	user_id = models.CharField(max_length = 30)
	full_name = models.CharField(max_length = 60)
	username = models.CharField(max_length = 32)
	email = models.EmailField()
	password = models.CharField(max_length = 32)
	address = models.TextField()
	bank = models.CharField(max_length = 40)
	bank_id = models.CharField(max_length = 20)