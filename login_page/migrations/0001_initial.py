# Generated by Django 2.2 on 2019-04-26 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.CharField(max_length=30)),
                ('full_name', models.CharField(max_length=60)),
                ('username', models.CharField(max_length=32)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(max_length=32)),
                ('address', models.TextField()),
                ('bank', models.CharField(max_length=40)),
                ('bank_id', models.CharField(max_length=20)),
            ],
        ),
    ]
