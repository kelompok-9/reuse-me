from django.test import TestCase
from .models import User

# Create your tests here.
class Test(TestCase):
	def is_exist(self):
		response = Client().get('login/')
		self.assertEqual(response.status_code,200)

	def right_html_used(self):
		response = Client.get('login/')
		self.assertTemplateUsed(response, 'login.html')

	def index_used(self):
		found = resolve('login/')
		self.assertEqual(found.func, index)

	def set_up_model(self):
		User.objects.create(
			user_id = '0123456789',
			full_name = 'Bambang Hermawan',
			username = 'bambanghermawan',
			email = 'bambanghermawan444@ui.ac.id',
			password = 'test123',
			address = '''Universitas Indonesia
Kampus UI Depok 16424, Jawa Barat Indonesia''',
			bank = 'Bank Central Asia',
			bank_id = '9876543210'
		)
	def testing_model(self):
		user_test = User.objects.get(user_id= '0123456789')
		self.assertEqual(user_test.full_name, 'Bambang Hermawan')
		self.assertEqual(user_test.username, 'bambanghermawan')
		self.assertEqual(user_test.email, 'bambanghermawan444@ui.ac.id')
		self.assertEqual(user_test.password, 'test123')
		self.assertEqual(user_test.address, '''Universitas Indonesia
Kampus UI Depok 16424, Jawa Barat Indonesia''')
		self.assertEqual(user_test.bank, 'Bank Central Asia')
		self.assertEqual(user_test.bank_id, '9876543210')