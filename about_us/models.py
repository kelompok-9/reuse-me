from django.db import models

class Testimony(models.Model):
    content = models.TextField()
    username = models.CharField(max_length=255)