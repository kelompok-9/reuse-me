from django.urls import path

from crowdfunding import views

app_name = "about_us"

urlpatterns = [
    path('testimony-api/', views.testimony_api, name='testimony-api'),
    path('about/', views.about, name='about'),
]