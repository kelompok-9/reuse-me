from django.test import TestCase

# Create your tests here.
class Test(TestCase):
	def is_exist(self):
		response = Client().get('sell/')
		self.assertEqual(response.status_code,200)

	def right_html_used(self):
		response = Client.get('sell/')
		self.assertTemplateUsed(response, 'sell.html')

	def index_used(self):
		found = resolve('sell/')
		self.assertEqual(found.func, index)
