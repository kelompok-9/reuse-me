from django.urls import path
from .views import sell

urlpatterns = [
	path('', sell, name='sell'),
]