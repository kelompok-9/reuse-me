from django.apps import AppConfig


class SellPageConfig(AppConfig):
    name = 'sell_page'
