# Generated by Django 2.2 on 2019-04-26 12:33

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('login_page', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Barang',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('categories', models.CharField(choices=[('fashion', 'Fashion'), ('perlengkaan_rumah', 'Perlengkaan Rumah'), ('elektronik_gadget', 'Elektronik dan Gadget'), ('sport_hobi', 'Sport dan Hobi'), ('other', 'Lainnya')], default='other', max_length=30)),
                ('nama_barang', models.CharField(max_length=50)),
                ('picture', models.ImageField(upload_to='')),
                ('description', models.TextField()),
                ('price', models.IntegerField()),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('email', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='login_page.User')),
                ('username', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='login_page.User')),
            ],
        ),
    ]
