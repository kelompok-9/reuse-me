from django.test import TestCase
from .models import Barang
from login_page.models import User

# Create your tests here.
class Test(TestCase):
	def is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	def right_html_used(self):
		response = Client.get('')
		self.assertTemplateUsed(response, 'buy.html')

	def index_used(self):
		found = resolve('')
		self.assertEqual(found.func, index)

	date_test = timezone.now

	def setup_user_for_testing(self):
		User.objects.create(
			user_id = '0123456789',
			full_name = 'Bambang Hermawan',
			username = 'bambanghermawan',
			email = 'bambanghermawan444@ui.ac.id',
			password = 'test123',
			address = '''Universitas Indonesia
Kampus UI Depok 16424, Jawa Barat Indonesia''',
			bank = 'Bank Central Asia',
			bank_id = '9876543210'
		)

	def setup_barang_for_testing(self):
		user = User.objects.get(user_id = '0123456789')
		Barang.objects.create(
			username = user.username,
			email = user.email,
			categories = 'other',
			nama_barang = 'Gundam',
			picture = '',
			description = 'Dibeli gan, butuh uang. Koleksi mahal dari Antartika',
			price = 100000000,
			date = date_test, 
		)

	def test_Barang(self):
		user = User.objects.get(user_id = '0123456789')
		sample = Barang.objects.get(date = date_test)
		self.assertEqual(sample.username, user.username)
		self.assertEqual(sample.email, user.email)
		self.assertEqual(sample.categories, 'other')
		self.assertEqual(sample.nama_barang, 'Gundam')
		self.assertEqual(sample.picture, '')
		self.assertEqual(sample.description, 'Dibeli gan, butuh uang. Koleksi mahal dari Antartika')
		self.assertEqual(sample.price, 100000000)