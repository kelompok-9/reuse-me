from django.db import models
from django.utils import timezone
from login_page.models import User

# Create your models here.
# Data : Username, Email, Categories, Nama barang, Foto barang, Deskripsi, harga, date
# Kategori barang = Fashion, Perlengkaan Rumah, Elektronik & Gadget, sport dan hobi, lainnya)
class Barang(models.Model):
	CATEGORIES_CHOICES = (
			('fashion', 'Fashion'),
			('perlengkaan_rumah', 'Perlengkaan Rumah'),
			('elektronik_gadget', 'Elektronik dan Gadget'),
			('sport_hobi', 'Sport dan Hobi'),
			('other', 'Lainnya'),
		)

	username = models.ForeignKey(User, on_delete= models.CASCADE, related_name='+')
	email = models.ForeignKey(User, on_delete= models.CASCADE, related_name='+')
	categories = models.CharField(
		max_length = 30,
		choices = CATEGORIES_CHOICES,
		default = 'other',
		)
	nama_barang = models.CharField(max_length = 50)
	picture = models.ImageField()
	description = models.TextField()
	price = models.IntegerField()
	date = models.DateTimeField(default = timezone.now)