from django.apps import AppConfig


class BuyPageConfig(AppConfig):
    name = 'buy_page'
